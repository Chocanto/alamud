from .event import Event3

class SpeakEvent(Event3):
	NAME = "speak"

	def perform(self):
		if not self.object.has_prop("speakable"):
			self.fail()
			return self.inform("speak.failed")
		self.add_prop("speaked-"+self.object2)
		self.inform("speak")
